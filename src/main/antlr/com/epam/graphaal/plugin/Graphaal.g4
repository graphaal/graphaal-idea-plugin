grammar Graphaal;

@parser::header
{
// DO NOT MODIFY - generated from Graphaal.g4 using "mx create-graphaal-parser"

import com.epam.graphaal.GraphaalLanguage;
import com.epam.graphaal.nodes.GraphaalExpressionNode;
import com.epam.graphaal.nodes.GraphaalStatementNode;
import com.epam.graphaal.nodes.controlflow.GraphaalEchoPositionNode;
import com.oracle.truffle.api.RootCallTarget;
import com.oracle.truffle.api.source.Source;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
}

@lexer::header
{
// DO NOT MODIFY - generated from Graphaal.g4 using "mx create-graphaal-parser"
}

@parser::members
{
private GraphaalNodeFactory factory;

public static Map<String, RootCallTarget> parseGraphaal(GraphaalLanguage language, Source source) {
    GraphaalLexer lexer = new GraphaalLexer(CharStreams.fromString(source.getCharacters().toString()));
    GraphaalParser parser = new GraphaalParser(new CommonTokenStream(lexer));
    parser.factory = new GraphaalNodeFactory(language, source);
    parser.compilationUnit();
    return parser.factory.getAllSpaces();
}
}

// parser

compilationUnit
:
typeDeclaration typeDeclaration* EOF
;

typeDeclaration
:
(spaceDeclaration|graphDeclaration|vertexDeclaration|edgeDeclaration)
| SEMICOLON
;

spaceDeclaration
:
SPACE IDENTIFIER
{ factory.startSpace($IDENTIFIER); }
bodyNode=body { factory.finishSpace($bodyNode.result); }
;

graphDeclaration
:
GRAPH IDENTIFIER
{ factory.startGraph($IDENTIFIER); }
bodyNode=body { factory.finishGraph($bodyNode.result); }
;

vertexDeclaration
:
VERTEX IDENTIFIER
body
;

edgeDeclaration
:
EDGE IDENTIFIER
body
;

body returns [GraphaalStatementNode result]
:
s=CURLY_BRACE_OPEN
{ factory.startBlock();
List<GraphaalStatementNode> body = new ArrayList<>();}
(statement SEMICOLON { body.add($statement.result); })*
e=CURLY_BRACE_CLOSE
{ $result = factory.finishBlock(body, $s.getStartIndex(), $e.getStopIndex() - $s.getStartIndex() + 1); }
;

statement returns [GraphaalStatementNode result]
:
declarationStatement { $result = $declarationStatement.result; }
|
assignStatement { $result = $assignStatement.result; }
|
createStatement { $result = $createStatement.result; }
|
addRelationStatement { $result = $addRelationStatement.result; }
|
deleteStatement { $result = $deleteStatement.result; }
|
extractionStatement { $result = $extractionStatement.result; }
|
expression { $result = $expression.result; }
;

declarationStatement returns [GraphaalStatementNode result]
:
receiver{ $result = $receiver.result; }
;

receiver returns [GraphaalExpressionNode result]
:
IDENTIFIER { GraphaalExpressionNode assignmentName = factory.createStringLiteral($IDENTIFIER, false);
List<GraphaalExpressionNode> nestedAssignmentNames = new ArrayList<>();
GraphaalExpressionNode typeName = null;}
(DOT IDENTIFIER{ nestedAssignmentNames.add(factory.createStringLiteral($IDENTIFIER, false)); })*
(COLON IDENTIFIER{ typeName = factory.createStringLiteral($IDENTIFIER, false); })?
{$result = factory.createReceiver(assignmentName, nestedAssignmentNames, typeName);}
;

assignStatement returns [GraphaalStatementNode result]
:
receiver ASSIGN statement
{$result = factory.createAssignment($receiver.result, $statement.result);}
;

createStatement returns [GraphaalStatementNode result]
:
CREATE IDENTIFIER { String typeName = $IDENTIFIER.getText();
GraphaalExpressionNode targetObjectName = null;
List<GraphaalStatementNode> objectAttributes = new ArrayList<>();
}
(CURLY_BRACE_OPEN statement {objectAttributes.add($statement.result);}(COMMA statement {objectAttributes.add($statement.result);})* CURLY_BRACE_CLOSE)?
(IN IDENTIFIER { targetObjectName = factory.createStringLiteral($IDENTIFIER, false); })?
{$result = factory.createObject(typeName, objectAttributes);}
;

addRelationStatement returns [GraphaalStatementNode result]
:
ADD
{ GraphaalExpressionNode vertexLeft = null;
 GraphaalExpressionNode vertexRight = null; }
(IDENTIFIER {vertexLeft = factory.createStringLiteral($IDENTIFIER, false);})?
relation
(IDENTIFIER {vertexRight = factory.createStringLiteral($IDENTIFIER, false);})?
{$result = factory.addRelation($relation.result, vertexLeft, vertexRight);}
;

relation returns [GraphaalStatementNode result]
:
nonDirectionalRelation {$result = $nonDirectionalRelation.result;}
|
uniDirectionalRelation {$result = $uniDirectionalRelation.result;}
|
biDirectionalRelation {$result = $biDirectionalRelation.result;}
;

nonDirectionalRelation returns [GraphaalStatementNode result]
:
{GraphaalStatementNode edge = null;}
DASH (SQARE_BRACE_OPEN statement SQARE_BRACE_CLOSE {edge = $statement.result;})? DASH
{$result = factory.createRelation(edge, false, false);}
;

uniDirectionalRelation returns [GraphaalStatementNode result]
:
toLeftRelation {$result = $toLeftRelation.result;}
|
toRightRelation {$result = $toRightRelation.result;}
;

toLeftRelation returns [GraphaalStatementNode result]
:
{GraphaalStatementNode edge = null;}
ARROW_LEFT (SQARE_BRACE_OPEN statement SQARE_BRACE_CLOSE {edge = $statement.result;})* DASH
{$result = factory.createRelation(edge, true, false);}
;

toRightRelation returns [GraphaalStatementNode result]
:
{GraphaalStatementNode edge = null;}
DASH (SQARE_BRACE_OPEN statement SQARE_BRACE_CLOSE {edge = $statement.result;})* ARROW_RIGHT
{$result = factory.createRelation(edge, false, true);}
;

biDirectionalRelation returns [GraphaalStatementNode result]
:
{GraphaalStatementNode edge = null;}
ARROW_LEFT (SQARE_BRACE_OPEN statement SQARE_BRACE_CLOSE {edge = $statement.result;})* ARROW_RIGHT
{$result = factory.createRelation(edge, true, true);}
;

deleteStatement returns [GraphaalStatementNode result]
:
DELETE IDENTIFIER (FROM IDENTIFIER)?
;

extractionStatement returns [GraphaalStatementNode result]
:
IDENTIFIER OF IDENTIFIER
;

expression returns [GraphaalExpressionNode result]
:
(
    STRING_LITERAL { $result = factory.createStringLiteral($STRING_LITERAL, false); }
|
    NUMERIC_LITERAL { $result = factory.createNumericLiteral($NUMERIC_LITERAL); }
|
    s=BRACE_OPEN expr=expression e=BRACE_CLOSE { $result = factory.createParenExpression($expr.result, $s.getStartIndex(), $e.getStopIndex() - $s.getStartIndex() + 1); }
)
;

//lexer

WS : [ \t\r\n\u000C]+ -> channel(HIDDEN);
COMMENT : '/*' .*? '*/' -> channel(HIDDEN);
LINE_COMMENT : '//' ~[\r\n]* -> channel(HIDDEN);

SPACE: 'Space';
GRAPH: 'Graph';
VERTEX: 'Vertex';
EDGE: 'Edge';

CREATE: 'Create';
ADD: 'Add';
DELETE: 'Delete';

FROM: 'from';
OF: 'of';
IN: 'in';

DOT: '.';
COMMA: ',';
COLON: ':';
SEMICOLON: ';';
ASSIGN: '=';
BRACE_OPEN: '(';
BRACE_CLOSE: ')';
CURLY_BRACE_OPEN: '{';
CURLY_BRACE_CLOSE: '}';
SQARE_BRACE_OPEN: '[';
SQARE_BRACE_CLOSE: ']';
DASH: '-';
ARROW_LEFT: '<-';
ARROW_RIGHT: '->';

fragment LETTER : [A-Z] | [a-z] | '_' | '$';
fragment NON_ZERO_DIGIT : [1-9];
fragment DIGIT : [0-9];
fragment HEX_DIGIT : [0-9] | [a-f] | [A-F];
fragment OCT_DIGIT : [0-7];
fragment BINARY_DIGIT : '0' | '1';
fragment TAB : '\t';
fragment STRING_CHAR : ~('"' | '\\' | '\r' | '\n');

IDENTIFIER : LETTER (LETTER | DIGIT)*;
STRING_LITERAL : '"' STRING_CHAR* '"';
NUMERIC_LITERAL : '0' | NON_ZERO_DIGIT DIGIT*;
