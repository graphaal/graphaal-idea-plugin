package com.epam.graphaal.plugin;

import com.intellij.lang.Language;

public class GraphaalLanguage  extends Language{
    public static final String ID = "Graphaal";
    public static final GraphaalLanguage INSTANCE = new GraphaalLanguage();

    private GraphaalLanguage() {
        super(ID);
    }
}
