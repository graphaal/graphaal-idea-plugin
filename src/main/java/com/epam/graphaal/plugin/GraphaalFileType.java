package com.epam.graphaal.plugin;

import com.intellij.openapi.fileTypes.LanguageFileType;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import javax.swing.*;

public class GraphaalFileType extends LanguageFileType {
    public static final String FILE_EXTENSION = "graphaal";
    public static final GraphaalFileType INSTANCE = new GraphaalFileType();

    protected GraphaalFileType() {
        super(GraphaalLanguage.INSTANCE);
    }

    @NotNull
    @Override
    public String getName() {
        return "Graphaal file";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Graphaal language file";
    }

    @NotNull
    @Override
    public String getDefaultExtension() {
        return FILE_EXTENSION;
    }

    @Nullable
    @Override
    public Icon getIcon() {
        return GraphaalIcons.GRAPHAAL_ICON;
    }
}
