package com.epam.graphaal.plugin;

import com.intellij.openapi.util.IconLoader;

import javax.swing.*;

public class GraphaalIcons {
	public static final Icon GRAPHAAL_ICON = IconLoader.getIcon("/com/epam/graphaal/plugin/sample.png");
	public static final Icon FUNC_ICON = IconLoader.getIcon("/com/epam/graphaal/plugin/f.png");
}
