package com.epam.graphaal.plugin;

import com.intellij.lexer.Lexer;
import com.intellij.openapi.editor.DefaultLanguageHighlighterColors;
import com.intellij.openapi.editor.colors.TextAttributesKey;
import com.intellij.openapi.fileTypes.SyntaxHighlighterBase;
import com.intellij.psi.tree.IElementType;
import org.antlr.intellij.adaptor.lexer.ANTLRLexerAdaptor;
import org.antlr.intellij.adaptor.lexer.PSIElementTypeFactory;
import org.antlr.intellij.adaptor.lexer.TokenIElementType;
import com.epam.graphaal.parser.GraphaalLexer;
import org.jetbrains.annotations.NotNull;

import static com.intellij.openapi.editor.colors.TextAttributesKey.createTextAttributesKey;
import static com.epam.graphaal.parser.GraphaalParser.*;

public class GraphaalSyntaxHighlighter extends SyntaxHighlighterBase {
    private static final TextAttributesKey[] EMPTY_KEYS = new TextAttributesKey[0];

    public static final TextAttributesKey IDENTIFIER =
            createTextAttributesKey("GRAPHAAL_ID", DefaultLanguageHighlighterColors.IDENTIFIER);
    public static final TextAttributesKey KEYWORD =
            createTextAttributesKey("GRAPHAAL_KEYWORD", DefaultLanguageHighlighterColors.KEYWORD);
    public static final TextAttributesKey STRING =
            createTextAttributesKey("GRAPHAAL_STRING", DefaultLanguageHighlighterColors.STRING);
    public static final TextAttributesKey LINE_COMMENT =
            createTextAttributesKey("GRAPHAAL_LINE_COMMENT", DefaultLanguageHighlighterColors.LINE_COMMENT);
    public static final TextAttributesKey BLOCK_COMMENT =
            createTextAttributesKey("GRAPHAAL_BLOCK_COMMENT", DefaultLanguageHighlighterColors.BLOCK_COMMENT);
    public static final TextAttributesKey OPERATION_SIGN =
            createTextAttributesKey("GRAPHAAL_OPERATION_SIGN", DefaultLanguageHighlighterColors.OPERATION_SIGN);
    public static final TextAttributesKey BRACES =
            createTextAttributesKey("GRAPHAAL_BRACES", DefaultLanguageHighlighterColors.BRACES);
    public static final TextAttributesKey SEMICOLON =
            createTextAttributesKey("GRAPHAAL_SEMICOLON", DefaultLanguageHighlighterColors.SEMICOLON);
    public static final TextAttributesKey DOT =
            createTextAttributesKey("GRAPHAAL_DOT", DefaultLanguageHighlighterColors.DOT);
    public static final TextAttributesKey COMMA =
            createTextAttributesKey("GRAPHAAL_COMMA", DefaultLanguageHighlighterColors.COMMA);
    public static final TextAttributesKey METADATA =
            createTextAttributesKey("GRAPHAAL_METADATA", DefaultLanguageHighlighterColors.METADATA);

    static {
        PSIElementTypeFactory.defineLanguageIElementTypes(GraphaalLanguage.INSTANCE,
                tokenNames,
                ruleNames);
    }

    @NotNull
    @Override
    public Lexer getHighlightingLexer() {
        GraphaalLexer lexer = new GraphaalLexer(null);
        return new ANTLRLexerAdaptor(GraphaalLanguage.INSTANCE, lexer);
    }

    @NotNull
    @Override
    public TextAttributesKey[] getTokenHighlights(IElementType tokenType) {
        if (!(tokenType instanceof TokenIElementType)) return EMPTY_KEYS;
        TokenIElementType myType = (TokenIElementType) tokenType;
        int ttype = myType.getANTLRTokenType();
        TextAttributesKey attrKey;
        switch (ttype) {
            case GraphaalLexer.IDENTIFIER:
                attrKey = IDENTIFIER;
                break;
	/*		case GraphaalLexer.VAR :
			case GraphaalLexer.WHILE :
			case GraphaalLexer.IF :
			case GraphaalLexer.ELSE :
			case GraphaalLexer.RETURN :
			case GraphaalLexer.PRINT :
			case GraphaalLexer.FUNC :
			case GraphaalLexer.TYPEINT :
			case GraphaalLexer.TYPEFLOAT :
			case GraphaalLexer.TYPESTRING :
			case GraphaalLexer.TYPEBOOLEAN :
			*/
            case GraphaalLexer.GRAPH:
            case GraphaalLexer.SPACE:
            case GraphaalLexer.VERTEX:
            case GraphaalLexer.EDGE:
            case GraphaalLexer.ADD:
            case GraphaalLexer.DELETE:
            case GraphaalLexer.FROM:
            case GraphaalLexer.CREATE:
            case GraphaalLexer.IN:
                attrKey = KEYWORD;
                break;
            case GraphaalLexer.ASSIGN:
                attrKey = OPERATION_SIGN;
                break;
            case GraphaalLexer.DASH:
            case GraphaalLexer.ARROW_LEFT:
            case GraphaalLexer.ARROW_RIGHT:
            case GraphaalLexer.SQARE_BRACE_CLOSE:
            case GraphaalLexer.SQARE_BRACE_OPEN:
                attrKey = METADATA;
                break;
            case GraphaalLexer.CURLY_BRACE_OPEN:
            case GraphaalLexer.CURLY_BRACE_CLOSE:
            case GraphaalLexer.BRACE_OPEN:
            case GraphaalLexer.BRACE_CLOSE:
                attrKey = BRACES;
                break;
            case GraphaalLexer.COLON:
            case GraphaalLexer.DOT:
                attrKey = DOT;
                break;
            case GraphaalLexer.COMMA:
                attrKey = COMMA;
                break;
            case GraphaalLexer.SEMICOLON:
                attrKey = SEMICOLON;
                break;
            case GraphaalLexer.STRING_LITERAL:
                attrKey = STRING;
                break;
            case GraphaalLexer.COMMENT:
                attrKey = LINE_COMMENT;
                break;
            case GraphaalLexer.LINE_COMMENT:
                attrKey = BLOCK_COMMENT;
                break;
            default:
                return EMPTY_KEYS;
        }
        return new TextAttributesKey[]{attrKey};
    }
}
